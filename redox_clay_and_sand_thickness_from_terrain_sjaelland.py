import os
import glob
from datetime import datetime
import time

import rasterio
from rasterio.warp import calculate_default_transform, Resampling, reproject
import numpy as np


def update_raster_projection(src_raster_path, dst_raster_path, dst_crs):
    # Open the input raster and read its metadata
    with rasterio.open(src_raster_path) as src:
        # Calculate the transformation parameters from the source to the target CRS
        transform, width, height = calculate_default_transform(src.crs, dst_crs, src.width, src.height, *src.bounds)

        # Update the metadata with the new transformation parameters
        dst_meta = src.meta.copy()
        dst_meta.update(
            {
                'crs': dst_crs,
                'transform': transform,
                'width': width,
                'height': height
            }
        )

        # Create the output raster file and reproject the input raster to the target CRS
        with rasterio.open(dst_raster_path, 'w', **dst_meta) as dst:
            for i in range(1, src.count + 1):
                reproject(
                    source=rasterio.band(src, i),
                    destination=rasterio.band(dst, i),
                    src_transform=src.transform,
                    src_crs=src.crs,
                    dst_transform=transform,
                    dst_crs=dst_crs,
                    resampling=Resampling.nearest
                )


def update_array_with_layer_top(src_array, top_ele_path, bot_ele_path, dem_ele_path):

    dem_ele = rasterio.open(dem_ele_path)

    with rasterio.open(top_ele_path) as top_ele, rasterio.open(bot_ele_path) as bot_ele:
        assert top_ele.meta == bot_ele.meta, "The metadata of the top and bot rasters are not equal"
        assert dem_ele.meta == top_ele.meta, "The metadata of the dem and top rasters are not equal"
        assert dem_ele.meta == bot_ele.meta, "The metadata of the dem and bot rasters are not equal"

        # Read in the data and metadata for each file
        top_ele_array = top_ele.read(1)
        bot_ele_array = bot_ele.read(1)
        dem_ele_array = dem_ele.read(1)

        top_depth_data = dem_ele_array - top_ele_array

        # find index where the input array is empty and where layer-thickness is above 1
        mask = np.logical_and(np.isnan(src_array), (top_ele_array - bot_ele_array) > 1)

        # update input array with layer top using the created index
        dst_array = np.where(mask, top_depth_data, src_array)

        return dst_array


def loop_raster_layers(raster_path_list, dem_ele_path, projectname):
    now = datetime.now()  # current date and time
    timestamp = now.strftime("%Y%m%d%H%M%S")

    # raster_path_list = sorted(glob.glob(f'{raster_root_path}\\*.grd'))
    dem = rasterio.open(dem_ele_path)

    # create np array for top clay and top sand containing empty values
    # note np.emptylike yielded an error => create a matrix of ones and then replacing the ones with nan
    depth_to_top_clay = np.ones_like(dem.read(1))
    depth_to_top_clay[:] = np.nan
    depth_to_top_sand = np.ones_like(dem.read(1))
    depth_to_top_sand[:] = np.nan

    # loop through all the raster files
    for i, raster_path in enumerate(raster_path_list):
        print(f'Loading {raster_path}...')

        filename = os.path.basename(raster_path)
        fohm_id = os.path.splitext(filename)[0]
        # fohm_id = filename[:4]

        bot_ele_path = raster_path
        top_ele_path = raster_path_list[i - 1]

        if fohm_id in fohm_id_not_sand:
            depth_to_top_clay = update_array_with_layer_top(depth_to_top_clay, top_ele_path, bot_ele_path, dem_ele_path)

        if fohm_id in fohm_id_not_clay:
            depth_to_top_sand = update_array_with_layer_top(depth_to_top_sand, top_ele_path, bot_ele_path, dem_ele_path)

    # ensure that all empty cell are set to desired output nodata value
    nodata = -9999  # GEUS' nodata value from the guideline per date 20230502
    depth_to_top_clay = np.where(np.isnan(depth_to_top_clay), nodata, depth_to_top_clay)
    depth_to_top_sand = np.where(np.isnan(depth_to_top_sand), nodata, depth_to_top_sand)

    # fetch the meta data and update the raster driver and the nodata value
    meta_output = dem.meta.copy()
    meta_output.update(
        {
            'driver': 'GTiff',
            'nodata': nodata
        }
    )

    # Write depth_to_top_clay as a geotif file
    depth_to_top_clay_filename = f'depth_to_top_clay_{projectname}_{timestamp}.tif'
    with rasterio.open(depth_to_top_clay_filename, 'w', **meta_output) as dst:
        dst.write(depth_to_top_clay, 1)

    # Write depth_to_top_sand as a geotif file
    depth_to_top_sand_filename = f'depth_to_top_sand_{projectname}_{timestamp}.tif'
    with rasterio.open(depth_to_top_sand_filename, 'w', **meta_output) as dst:
        dst.write(depth_to_top_sand, 1)

    # output the files in srid 32632 which is required be Geus
    dst_crs = 'EPSG:32632'
    depth_to_top_clay_filename_no_extension = os.path.splitext(depth_to_top_clay_filename)[0]
    depth_to_top_clay_filename_32632 = f'{depth_to_top_clay_filename_no_extension}_32632.tif'
    update_raster_projection(depth_to_top_clay_filename, depth_to_top_clay_filename_32632, dst_crs)

    depth_to_top_sand_filename_no_extension = os.path.splitext(depth_to_top_sand_filename)[0]
    depth_to_top_sand_filename_32632 = f'{depth_to_top_sand_filename_no_extension}_32632.tif'
    update_raster_projection(depth_to_top_sand_filename, depth_to_top_sand_filename_32632, dst_crs)


def order_sjaellands_layers(root_path_grid, fileextension=None):
    """
    quick fix for being able to use the existing script for FOHM layers
    :param root_path_grid: the root path containing all the grid files
    :param fileextension: the file extension of the grid files e.g. '.grd' or '.asc'
    :return: list of desired layer paths ordered from top layer to bottom layer
    """
    # find all layers
    if not fileextension:
        file_types = ('asc', 'grd', 'ASC', 'GRD')
        raster_path_list = []
        for file_type in file_types:
            raster_path_list.extend(glob.glob(f'{root_path_grid}\\{file_type}'))
    else:
        raster_path_list = glob.glob(f'{root_path_grid}\\*.{fileextension}')

    # define a filer of layers and omit grid-files that are not in the list
    layer = [
        'bund', 'dk1b',
        'gk1b', 'ks1b',
        'ks1t', 'ks2b',
        'ks2t', 'ks3b',
        'ks3t', 'ks4b',
        'ks4t', 'pl1b',
        'preq', 'topo'
    ]
    layer_list = []
    for raster_path in sorted(raster_path_list):
        filename = os.path.basename(raster_path)
        filename_noextension = os.path.splitext(filename)[0]
        if filename_noextension in layer:
            layer_list.append(raster_path)

    # define the correct order of the grid layers and order the grid-layers from top to bottom
    layer_index = [14, 13, 12, 3, 2, 5, 4, 7, 6, 9, 8, 11, 10, 1]
    raster_path_ordered = [x for _, x in sorted(zip(layer_index, layer_list))]

    return raster_path_ordered


# start timer
t = time.time()

# find the path of the pyproj lib from the conda env and set that path as windows environmental variable
conda_env_path = (os.environ["CONDA_PREFIX"])
env_path = f'{conda_env_path}/Lib/site-packages/pyproj/proj_dir/share/proj'
os.environ["PROJ_LIB"] = env_path

# EDIT HERE
dem_ele_path = r'C:\Users\b028067\Downloads\grids\topo.asc'
root_path_fohm_grid = r'C:\Users\b028067\Downloads\grids'
projectname = 'lolland'
file_extension = 'asc'

fohm_id_not_clay = [
    'ks1b',
    'ks2b',
    'ks3b',
    'ks4b',
    'gk1b',
    'dk1b',
    'bund'
]
fohm_id_not_sand = [
    'ks1t',
    'ks2t',
    'ks3t',
    'ks4t',
    'preq',
    'pl1b',
    'gk1b',
    'dk1b',
    'bund'
]
# raster_path_list = sorted(glob.glob(f'{root_path_fohm_grid}\\*.grd'))
raster_path_list = order_sjaellands_layers(root_path_fohm_grid, fileextension=file_extension)
# run function calculating depth to top clay and sand layer
loop_raster_layers(raster_path_list=raster_path_list, dem_ele_path=dem_ele_path, projectname=projectname)

# end timer and print time spend for execution
elapsed = round(time.time() - t, 2)
print(f'\n{os.path.basename(__file__)} executed in {elapsed} s')
