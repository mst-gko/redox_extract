import glob
import os
import time

import gdal
import rasterio
import numpy as np

__title__ = 'Redox - top sand and clay thickness'
__authors__ = 'Simon Makwarth <simak@mst.dk>'
__version__ = "0.0.1"
__license__ = "GPLv3"
__maintainer__ = "Simon Makwarth <simak@mst.dk>"
__repository__ = f"https://gitlab.com/mst-gko/redox_extract"
__issues__ = f'{__repository__}/-/issues'
__description__ = f'''{__title__} inputs fohm layer grids and output the thickness of sand and clay respectively. 
 E.g. The clay thickness is measured from terrain until til first non-clay layer (sand/limestone). 
The thickness of the underlying non-clay must be above 1 meter'''


class FohmRaster:

    def __init__(self, root_path, dem_filename, crop_raster=True):
        self.crop_raster = crop_raster
        self.root_path = root_path
        self.raster_paths = sorted(glob.glob(f'{self.root_path}\\*.grd'))
        self.dem_filename = dem_filename
        self.dem_path = f'{self.root_path}\\{self.dem_filename}'
        self.dem_rast = rasterio.open(self.dem_path)
        self.dem_array = self.dem_rast.read()
        self.dem_left, self.dem_top, self.dem_window = self.fetch_extent_from_raster()
        self.timestr = time.strftime("%Y%m%d")

    @staticmethod
    def gdal_raster_to_numpy_array(gdal_rast, rasterband=1):
        array_rast = np.array(gdal_rast.GetRasterBand(rasterband).ReadAsArray())

        array_rast_2 = array_rast[np.newaxis, ...]
        np.insert(array_rast_2, 0, rasterband, axis=0)

        return array_rast_2

    def numpy_to_raster_file(self, array, dst_path, filetype='tif', xsize=100, ysize=100):
        """
        On fohm layers (20220630) this functions displays an error on some of the cells, when using srid 25832
        It is not known why, but output file is as exected.
        :param filetype: output file type, supported 'grd' and 'tif'
        :param array: numpy array
        :param xsize:
        :param ysize:
        """
        if filetype == 'tif':
            driver = 'GTiff'
        elif filetype == 'grd':
            driver = 'GS7BG'
        else:
            filetype = 'tif'
            driver = 'GTiff'

        transform = rasterio.transform.from_origin(
            west=self.dem_left,
            north=self.dem_top,
            xsize=xsize,
            ysize=ysize
        )
        dst_path_full = f'{dst_path}_{self.timestr}.{filetype}'

        with rasterio.open(
            dst_path_full,
            'w',
            driver=driver,
            height=array.shape[1],
            width=array.shape[2],
            count=1,
            dtype=str(array.dtype),
            crs='EPSG:25832',
            transform=transform
        ) as rast:
            rast.write(array)

    def fetch_extent_from_raster(self):
        left = self.dem_rast.bounds.left
        top = self.dem_rast.bounds.top
        bottom = self.dem_rast.bounds.bottom
        right = self.dem_rast.bounds.right
        window = (left, top, right, bottom)

        return left, top, window

    def crop_raster_by_extent(self, raster_to_be_clipped_path):
        raster_to_be_clipped = gdal.Open(raster_to_be_clipped_path)
        raster_cropped = gdal.Translate('', raster_to_be_clipped, projWin=self.dem_window, format='MEM')

        return raster_cropped

    def create_empty_output_arr(self):
        output_array = np.empty(shape=self.dem_rast.shape)
        output_array[:] = np.NaN

        return output_array

    def update_array_with_layer_depth(self, output_array, layer_top_ele, layer_bot_ele):
        layer_top_depth = self.dem_array - layer_top_ele
        layer_thickness = layer_top_ele - layer_bot_ele

        # check if output
        output_array = np.where(
            np.isnan(output_array),
            np.where(
                layer_thickness > 1,
                layer_top_depth,
                output_array
            ),
            output_array
        )

        return output_array

    def raster_depth_to_elevation(self, arr_depth):
        arr_ele = arr_depth + self.dem_array

        return arr_ele

    def loop_raster_files(self):
        fohm_id_not_sand = [
            '0100', '0300', '1100', '1300', '1500',
            '2200', '2400', '5100', '5300', '5500',
            '5700', '5900', '6100', '6300', '6500',
            '6700', '6900', '7100', '7300', '7500',
            '7700', '8000', '8500', '9000', '9500'
        ]

        fohm_id_not_clay = [
            '0050', '0200', '0400', '1200', '1400',
            '2100', '2300', '5200', '5400', '5600',
            '5800', '6000', '6200', '6400', '6600',
            '6800', '7000', '7200', '7400', '7600',
            '7800', '8500', '9000', '9500'
        ]

        array_top_not_sand = self.create_empty_output_arr()
        array_top_not_clay = self.create_empty_output_arr()

        for i, raster_path in enumerate(self.raster_paths):
            print(f'Loading {raster_path}...')
            filename = os.path.basename(raster_path)
            fohm_id = filename[:4]

            if fohm_id in fohm_id_not_sand:
                layer_bot_cropped = self.crop_raster_by_extent(raster_path)
                layer_top_cropped = self.crop_raster_by_extent(self.raster_paths[i - 1])

                layer_bot_ele = self.gdal_raster_to_numpy_array(layer_bot_cropped)
                layer_top_ele = self.gdal_raster_to_numpy_array(layer_top_cropped)

                array_top_not_sand = self.update_array_with_layer_depth(
                    array_top_not_sand,
                    layer_top_ele,
                    layer_bot_ele,
                )

            if fohm_id in fohm_id_not_clay:
                layer_bot_cropped = self.crop_raster_by_extent(raster_path)
                layer_top_cropped = self.crop_raster_by_extent(self.raster_paths[i - 1])

                layer_bot_ele = self.gdal_raster_to_numpy_array(layer_bot_cropped)
                layer_top_ele = self.gdal_raster_to_numpy_array(layer_top_cropped)

                array_top_not_clay = self.update_array_with_layer_depth(
                    array_top_not_clay,
                    layer_top_ele,
                    layer_bot_ele,
                )

        return array_top_not_sand, array_top_not_clay


def run_redox_fohm_depth(root_path, dem_filename, crop_raster=True, make_gs3d_files=True):
    fr = FohmRaster(root_path=root_path, crop_raster=crop_raster, dem_filename=dem_filename)
    array_thickness_sand, array_thickness_clay = fr.loop_raster_files()

    filename_clay = 'uninterrupted_clay_thickness_from_terrain'
    print(f'\nCreating file: {filename_clay}..')
    fr.numpy_to_raster_file(array=array_thickness_clay, dst_path=filename_clay)

    filename_sand = 'uninterrupted_sand_thickness_from_terrain'
    print(f'\nCreating file: {filename_sand}..')
    fr.numpy_to_raster_file(array=array_thickness_sand, dst_path=filename_sand)

    if make_gs3d_files:
        print('\nCreating raster files as elevation for geoscene3d...')
        array_thickness_sand_ele = fr.raster_depth_to_elevation(array_thickness_clay)
        fr.numpy_to_raster_file(array=array_thickness_sand_ele, dst_path=f'{filename_clay}_elevation', filetype='grd')
        array_thickness_sand_ele = fr.raster_depth_to_elevation(array_thickness_sand)
        fr.numpy_to_raster_file(array=array_thickness_sand_ele, dst_path=f'{filename_sand}_elevation', filetype='grd')


# start timer
t = time.time()

# EDIT HERE, path for all fohm grids, e.g. 'C:\path\to\folder'
root_path = r'C:\Users\b028067\Downloads\HSM_flader_50'

# EDIT HERE, filename of the dem grid, e.g. 'dem_filename.grd'
dem_filename = '0001_DHM_geometri_50.grd'

run_redox_fohm_depth(root_path, dem_filename)

# end timer and print time spend for execution
elapsed = round(time.time() - t, 2)
print(f'\n{os.path.basename(__file__)} executed in {elapsed} s')
