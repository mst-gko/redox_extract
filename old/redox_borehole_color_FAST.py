from __future__ import print_function
import os
import time
from io import BytesIO
import warnings

import pdfkit as pdf
import requests
from PyPDF2 import PdfFileMerger, PdfFileReader
import pandas as pd
# import openpyxl  # Needed for pandas to_excel command
from openpyxl.styles import PatternFill
from openpyxl import load_workbook
import shapefile  # conda install pyshp
import pygeoif
import psycopg2 as pg
import configparser

__title__ = 'Redox Extract'
__authors__ = 'Simon Makwarth <simak@mst.dk>'
__version__ = "1.0.0"
__license__ = "GPLv3"
__maintainer__ = "Simon Makwarth <simak@mst.dk>"
__repository__ = f"https://gitlab.com/mst-gko/{__title__.lower().replace(' ', '_')}"
__issues__ = f'{__repository__}/-/issues'
__description__ = f'''{__title__} extracts borehole information from jupiter database within a shape geometry 
that contains a borehole report. Furthermore the program extracts url from boreholereport, 
downloads them all and merges them into a single pdf.'''
__requirements__ = f'''
    - wkhtmltopdf.exe: https://wkhtmltopdf.org/
    - shapefile input: containing the area of interest using a geometry in crs 25832.
'''


# TODO  rewrite to avoid pdfkit lib and wkhtmltopdf.exe, still needs url link in jupitercolor (low prio)


class Database:
    def __init__(self, database, user):
        self.ini_dir = f'F:/GKO/data/grukos/db_credentials/{user.lower()}/{user.lower()}.ini'
        self.ini_section = database.upper()

    def parse_db_credentials(self):
        config = configparser.ConfigParser()
        config.read(self.ini_dir)

        db_connect_kwargs = {
            'dbname': f"{config[f'{self.ini_section}']['databasename']}",
            'user': f"{config[f'{self.ini_section}']['userid']}",
            'password': f"{config[f'{self.ini_section}']['password']}",
            'host': f"{config[f'{self.ini_section}']['host']}",
            'port': f"{config[f'{self.ini_section}']['port']}"
        }

        return db_connect_kwargs

    def connect_to_pg_db(self):
        db_connect_kwargs = self.parse_db_credentials()
        try:
            pg_con = pg.connect(**db_connect_kwargs)

        except Exception as e:
            print('Unable to connect to database: ' + db_connect_kwargs['dbname'])
            print(e)
        else:
            if db_connect_kwargs:
                del db_connect_kwargs

            if pg_con:
                return pg_con


class BoreholeColor:

    def __init__(self, path_shape_input):
        """

        :param path_shape_input: directory to the shapefile (*.shp)
        """
        self.path_shape_input = path_shape_input
        self.filename = os.path.splitext(os.path.basename(self.path_shape_input))[0]
        self.sql_borehole_out, self.sql_pdf_out, self.sql_jup_color_out = self.create_redox_sql()
        self.path_excel = f'./{self.filename}_redox_boringer.xlsx'
        self.path_pdf_boreholecolor = f"./{self.filename}_boreholecolor.pdf"
        self.path_pdf_jupitercolor = f'{self.filename}_jupiterredox'

    def create_redox_sql(self):
        """
        Transform a geometry fra a input shapefile as a string (sql select statement)
        :return: geom_wtk: sql string containing a select statement all wkt-strings from the geometry of the shapefile.
        """
        sf = shapefile.Reader(self.path_shape_input)
        shp_geom = ''
        for k, s in enumerate(sf.shapes()):
            g = pygeoif.geometry.as_shape(s)
            poly = str(pygeoif.MultiPolygon(g))
            if k == 0:
                shp_geom += f"""
                                SELECT 
                                1 as row_id,
                                ST_MakeValid(ST_GeomFromText('{poly}', 25832)) as geom"""
            elif k > 0:
                shp_geom += f""" 
                                UNION ALL 
                                SELECT
                                    1 as row_id,
                                    ST_MakeValid(ST_GeomFromText('{poly}', 25832)) as geom"""
            else:
                pass

        # define database query for borehole information in jupiter db
        sql_borehole_out = f"""
            WITH tmp as ({shp_geom})
            SELECT DISTINCT
                b.boreholeno as DGU_nr, b.xutm32euref89,
                b.yutm32euref89, b.elevation as Terraenkote,
                b.drilldepth as Boringsdybde, b.drilendate as Boringsdato,
                NULL AS Dbd_oeverste_redoxgraense,
                NULL AS kote,
                NULL AS Redoxtype,
                NULL AS Antal_farveskift,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Bemaerkning,
                NULL AS Dbd_1_red_ox,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Dbd_2_ox_red,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Dbd_2_red_ox,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Dbd_3_ox_red,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Dbd_3_red_ox,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Dbd_4_ox_red,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Dbd_4_red_ox,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Dbd_5_ox_red,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Dbd_5_red_ox,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Dbd_6_ox_red,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Dbd_6_red_ox,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval,
                NULL AS Dbd_7_ox_red,
                NULL AS Kote,
                NULL AS Type,
                NULL AS Top_af_interval,
                NULL AS Bund_af_interval
            FROM jupiter.borehole b
            INNER JOIN tmp ON st_within(b.geom, tmp.geom)
            LEFT JOIN jupiter.boredoc bd USING (boreholeid)
            LEFT JOIN jupiter.lithsamp ls USING (boreholeid)
            WHERE b.geom IS NOT NULL
                AND 
                    (
                        bd.doctype IN ('B', 'BG') 
                        OR COALESCE(ls.color, ls.munsellcolor, ls.oldcolor, ls.drillcolor) IS NOT NULL
                    )
            ORDER BY b.boreholeno     
        """
        print(sql_borehole_out)

        # define database query for borehole report urls in jupiter db
        sql_pdf_out = f'''
            WITH tmp as ({shp_geom})
            SELECT DISTINCT    
                sd.url, b.boreholeno, bd.versionno,  
                concat(b.boreholeno, '_', bd.doctype, '_', bd.versionno) as bor_ver 
            FROM jupiter.storedoc sd
            LEFT JOIN jupiter.boredoc bd USING (fileid)
            LEFT JOIN jupiter.borehole b USING (boreholeid) 
            INNER JOIN tmp ON st_within(b.geom, tmp.geom)
            WHERE b.geom IS NOT NULL
                AND bd.doctype IN ('B', 'BG')
            ORDER BY b.boreholeno, bd.versionno
            ;
        '''
        print(sql_pdf_out)

        # define database query for borehole color in jupiter db
        borid_url = 'https://data.geus.dk/JupiterWWW/borerapport.jsp?borid'
        sql_jup_color_out = f'''
            WITH tmp as ({shp_geom})
            SELECT DISTINCT
                '<a href="' || '{borid_url}=' || b.boreholeid || '">' || replace(b.boreholeno, ' ', '') || '</a>' as url, 
                b.boreholeid, b.boreholeno,
                ls.top, ls.bottom, ls.rocksymbol, 
                cd.color_descr,	cd1.drilcol_descr,
                cd2.oldcolor_descr,	cd3.muncolor_descr
            FROM jupiter.lithsamp ls
            INNER JOIN jupiter.borehole b ON b.boreholeid = ls.boreholeid
            INNER JOIN tmp ON st_within(b.geom, tmp.geom)
            LEFT JOIN jupiter.boredoc bd ON bd.boreholeid = ls.boreholeid 
            LEFT JOIN 
                (
                    SELECT c.code, c.longtext AS color_descr
                    FROM jupiter.code c 
                    WHERE c.codetype = 48
                ) AS cd ON cd.code = ls.color
            LEFT JOIN 
                (
                    SELECT c.code, c.longtext AS drilcol_descr
                    FROM jupiter.code c 
                    WHERE c.codetype = 48
                ) AS cd1 ON cd1.code = ls.drillcolor
            LEFT JOIN 
                (
                    SELECT c.code, c.longtext AS oldcolor_descr
                    FROM jupiter.code c 
                    WHERE c.codetype = 51
                ) AS cd2 ON cd2.code = ls.oldcolor
            LEFT JOIN 
                (
                    SELECT c.code, c.longtext AS muncolor_descr
                    FROM jupiter.code c 
                    WHERE c.codetype = 79
                ) AS cd3 ON cd3.code = ls.munsellcolor
            WHERE b.geom IS NOT NULL
                AND COALESCE(ls.color, ls.munsellcolor, ls.oldcolor, ls.drillcolor) IS NOT NULL
            ORDER BY b.boreholeno, ls.top 
            ;
        '''
        print(sql_jup_color_out)

        return sql_borehole_out, sql_pdf_out, sql_jup_color_out

    def run_redox_borehole_color(self):
        warnings.filterwarnings("ignore")

        # fetch database credentials
        db = Database(database='JUPITER', user='reader')
        # pguser, pgpassword, pghost, pgport, pgdatabase = parse_db_credentials(ini_dir, ini_section)

        # fetch sql to:
        #   1) borehole information for excel spreadsheet,
        #   2) borehole reports for combined pdf and
        #   3) jupiter color descr for pdf.
        sql_borehole, sql_pdf, sql_jup_color = self.create_redox_sql()

        # connect to the databases af extract dataframe from sql queries
        with db.connect_to_pg_db() as con:
            df_borehole = pd.read_sql_query(sql_borehole, con=con)
            df_pdf_url = pd.read_sql_query(sql_pdf, con=con)
            df_jup_color = pd.read_sql_query(sql_jup_color, con=con)

        # # # CREATE MS EXCEL SPREADSHEET CONTAINING BOREHOLE INFORMATION # # #

        df_borehole.to_excel(excel_writer=self.path_excel, sheet_name='Metadata_redoxpunkter', index=False)
        self.color_columnnames()  # COLOR COLUMNNAMES

        # # # CREATE PDF FROM COLOR DESCRIPTION IN JUPITER LITHSAMP # # #
        print('\nCompiling pdf from jupiter color descriptions.... \n')
        self.create_pdf_jupiter_color(df_jup_color)

        # # # CREATE MERGED PDF FROM BOREHOLE DESCRIPTION REPORTS # # #
        print('\nCompiling borehole reports to a single pdf... \n')
        urls = df_pdf_url['url']
        # dgu_versions = df_pdf_url['bor_ver']

        self.merge_borehole_pdf(urls)

    def merge_borehole_pdf(self, urls_input):

        merger = PdfFileMerger(strict=False)
        for i, url in enumerate(urls_input):
            counter = i + 1
            update_str = f'\r{counter} report out of {len(urls_input)} ' \
                         f'({round(counter / len(urls_input) * 100, 2)}%) url: {url}'
            print(
                update_str,
                end='',
                flush=True
            )
            remote_file = requests.get(url)
            memory_file = BytesIO(remote_file.content)
            pdf_file = PdfFileReader(memory_file, strict=False)
            merger.append(pdf_file)

        output_stream = open(self.path_pdf_boreholecolor, "wb")
        merger.write(output_stream)  # this works, receives warning from pycharm (BytesIO vs BinaryIO)
        output_stream.close()

    def create_pdf_jupiter_color(self, df_jup_color_tmp_input):
        """
        descr:  group by dguno. and insert empty row and columnnames for each group
                source: https://stackoverflow.com/a/59623626/6129515
        :param df_jup_color_tmp_input:
        :return:
        """
        df2 = pd.DataFrame([[''] * len(df_jup_color_tmp_input.columns), df_jup_color_tmp_input.columns],
                           columns=df_jup_color_tmp_input.columns)
        df_jup_color = (
            df_jup_color_tmp_input.groupby('boreholeno', group_keys=False, sort=False)
            .apply(lambda d: d.append(df2))
            .iloc[:-2]
            .reset_index(drop=True)
        )

        path_wkhtmltopdf = './requirements/wkhtmltopdf/bin/wkhtmltopdf.exe'
        pdf_config = pdf.configuration(wkhtmltopdf=path_wkhtmltopdf)

        pdf_options = {
            'page-size': 'Letter',
            'margin-top': '0.75in',
            'margin-right': '0.75in',
            'margin-bottom': '0.75in',
            'margin-left': '0.75in',
            'encoding': "UTF-8",
            'custom-header': [
                ('Accept-Encoding', 'gzip')
            ],
            'cookie': [
                ('cookie-name1', 'cookie-value1'),
                ('cookie-name2', 'cookie-value2'),
            ],
            'no-outline': None
        }

        html = df_jup_color.to_html(escape=False)  # escape=false keeps the html encoding of the urls

        with open(f'./{self.path_pdf_jupitercolor}.html', "w", encoding="utf-8") as file:
            file.write(html)
        pdf.from_file(
            f'./{self.path_pdf_jupitercolor}.html', f'./{self.path_pdf_jupitercolor}.pdf',
            configuration=pdf_config,
            options=pdf_options
        )

        if os.path.exists(f'./{self.path_pdf_jupitercolor}.html'):
            os.remove(f'./{self.path_pdf_jupitercolor}.html')

    def color_columnnames(self):
        # wb = openpyxl.load_workbook(filename=self.path_excel)
        wb = load_workbook(filename=self.path_excel)
        ws = wb['Metadata_redoxpunkter']

        cell_color_dict = {
            'FF7DF26A': [
                'A1', 'B1', 'C1', 'D1', 'E1', 'F1'
            ],
            'FFFFE699': [
                'G1', 'H1', 'I1', 'J1', 'K1', 'L1', 'M1'
            ],
            'FFFCE4D6': [
                'S1', 'T1', 'U1', 'V1', 'W1',
                'AC1', 'AD1', 'AE1', 'AF1', 'AG1',
                'AM1', 'AN1', 'AO1', 'AP1', 'AQ1',
                'AW1', 'AX1', 'AY1', 'AZ1', 'BA1',
                'BG1', 'BH1', 'BI1', 'BJ1', 'BK1',
                'BQ1', 'BR1', 'BS1', 'BT1', 'BU1'
            ],
            'FFD0CECE': [
                'N1', 'O1', 'P1', 'Q1', 'R1',
                'X1', 'Y1', 'Z1', 'AA1', 'AB1',
                'AH1', 'AI1', 'AJ1', 'AK1', 'AL1',
                'AR1', 'AS1', 'AT1', 'AU1', 'AV1',
                'BB1', 'BC1', 'BD1', 'BE1', 'BF1',
                'BL1', 'BM1', 'BN1', 'BO1', 'BP1',
            ],
        }
        for color, cell_list in cell_color_dict.items():

            # colorfill = openpyxl.styles.PatternFill(
            colorfill = PatternFill(
                start_color=f'{color}',
                end_color=f'{color}',
                fill_type='solid'
            )

            for cell in cell_list:
                ws[f'{cell}'].fill = colorfill

        wb.save(self.path_excel)
        wb.close()


# start timer
t = time.time()

# EDIT SHAPE PATH HERE
path_shape = r'C:\Users\b028067\Desktop\temp\Polygon Mølleå redox boringer NORD.shp'
# C:\Users\b028067\Desktop\temp\Polygon Mølleå redox boringer SYD.shp

bc = BoreholeColor(path_shape)
bc.run_redox_borehole_color()

# end timer
elapsed = round(time.time() - t, 2)
print(f'\n{os.path.basename(__file__)} executed in {elapsed} s')
