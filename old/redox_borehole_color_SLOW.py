from __future__ import print_function
import os
import time
from io import BytesIO, SEEK_SET, SEEK_END
import warnings
import shutil

import pdfkit as pdf
import requests
from PyPDF2 import PdfFileMerger, PdfFileReader
import pandas as pd
import openpyxl  # Needed for pandas to_excel command
# from openpyxl.styles import PatternFill
import shapefile  # conda install pyshp
import pygeoif
import psycopg2 as pg
import configparser


__title__ = 'Redox Extract'
__authors__ = 'Simon Makwarth <simak@mst.dk>'
__version__ = "0.0.2"
__license__ = "GPLv3"
__maintainer__ = "Simon Makwarth <simak@mst.dk>"
__repository__ = f"https://gitlab.com/mst-gko/{__title__.lower().replace(' ', '_')}"
__issues__ = f'{__repository__}/-/issues'
__description__ = f'''{__title__} extracts borehole information from jupiter database within a shape geometry 
that contains a borehole report. Furthermore the program extracts url from boreholereport, 
downloads them all and merges them into a single pdf.'''
__requirements__ = f'''
    - wkhtmltopdf.exe: https://wkhtmltopdf.org/
    - shapefile input: containing the area of interest using a geometry in crs 25832.
'''

# TODO rewrite the many function into classes (low prio)
# TODO change script so only one pdf python library is needed (low prio)
# TODO rewrite function merge_borehole_pdf to not having to copy pdf onto disk while script avoids merging corrupted pdf

class ResponseStream(object):
    """
    This script takes a pdf url-response and makes it possible to store the content of the pdf without saving the pdf
    Source: https://www.geeksforgeeks.org/merge-pdf-stored-in-remote-server-using-python/
    """
    def __init__(self, request_iterator):
        self._bytes = BytesIO()
        self._iterator = request_iterator

    def _load_all(self):
        self._bytes.seek(0, SEEK_END)

        for chunk in self._iterator:
            self._bytes.write(chunk)

    def _load_until(self, goal_position):
        current_position = self._bytes.seek(0, SEEK_END)

        while current_position < goal_position:
            try:
                current_position = self._bytes.write(next(self._iterator))

            except StopIteration:
                break

    def tell(self):
        return self._bytes.tell()

    def read(self, size=None):
        left_off_at = self._bytes.tell()

        if size is None:
            self._load_all()
        else:
            goal_position = left_off_at + size
            self._load_until(goal_position)

        self._bytes.seek(left_off_at)

        return self._bytes.read(size)

    def seek(self, position, whence=SEEK_SET):

        if whence == SEEK_END:
            self._load_all()
        else:
            self._bytes.seek(position, whence)

        # Merge PDFs using URL List


def connect_to_pg_db(pghost_input, pgport_input, pgdatabase_input, pguser_input, pgpassword_input):
    try:
        pg_con = pg.connect(
            host=pghost_input,
            port=pgport_input,
            database=pgdatabase_input,
            user=pguser_input,
            password=pgpassword_input
        )
        print('Connected to database: ' + pgdatabase_input)
        return pg_con
    except Exception as e:
        print('Unable to connect to database: ' + pgdatabase_input)
        print(e)


def parse_db_credentials(ini_file, ini_section):
    config = configparser.ConfigParser()
    config.read(ini_file)

    user = config[f'{ini_section}']['userid']
    password = config[f'{ini_section}']['password']
    host = config[f'{ini_section}']['host']
    port = config[f'{ini_section}']['port']
    database = config[f'{ini_section}']['databasename']

    return user, password, host, port, database


def create_redox_sql(path_shape_input):
    """
    Transform a geometry fra a input shapefile as a string (sql select statement)
    :param path_shape_input: directory to the shapefile (*.shp)
    :return: geom_wtk: sql string containing a select statement all wkt-strings from the geometry of the shapefile.
    """
    sf = shapefile.Reader(path_shape_input)
    shp_geom = ''
    for k, s in enumerate(sf.shapes()):
        g = pygeoif.geometry.as_shape(s)
        poly = str(pygeoif.MultiPolygon(g))
        if k == 0:
            shp_geom += f"""
                            SELECT 
                            1 as row_id,
                            ST_MakeValid(ST_GeomFromText('{poly}', 25832)) as geom"""
        elif k > 0:
            shp_geom += f""" 
                            UNION ALL 
                            SELECT
                                1 as row_id,
                                ST_MakeValid(ST_GeomFromText('{poly}', 25832)) as geom"""
        else:
            pass

    # define database query for borehole information in jupiter db
    sql_borehole_out = f"""
        WITH tmp as ({shp_geom})
        SELECT DISTINCT
            b.boreholeno as DGU_nr, b.xutm32euref89,
            b.yutm32euref89, b.elevation as Terraenkote,
            b.drilldepth as Boringsdybde, b.drilendate as Boringsdato,
            NULL AS Dbd_oeverste_redoxgraense,
            NULL AS kote,
            NULL AS Redoxtype,
            NULL AS Antal_farveskift,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Bemaerkning,
            NULL AS Dbd_1_red_ox,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Dbd_2_ox_red,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Dbd_2_red_ox,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Dbd_3_ox_red,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Dbd_3_red_ox,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Dbd_4_ox_red,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Dbd_4_red_ox,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Dbd_5_ox_red,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Dbd_5_red_ox,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Dbd_6_ox_red,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Dbd_6_red_ox,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval,
            NULL AS Dbd_7_ox_red,
            NULL AS Kote,
            NULL AS Type,
            NULL AS Top_af_interval,
            NULL AS Bund_af_interval
        FROM jupiter.borehole b
        INNER JOIN tmp ON st_within(b.geom, tmp.geom)
        LEFT JOIN jupiter.boredoc bd USING (boreholeid)
        LEFT JOIN jupiter.lithsamp ls USING (boreholeid)
        WHERE b.geom IS NOT NULL
            AND 
                (
                    bd.doctype IN ('B', 'BG') 
                    OR COALESCE(ls.color, ls.munsellcolor, ls.oldcolor, ls.drillcolor) IS NOT NULL
                )
        ORDER BY b.boreholeno     
    """
    print(sql_borehole_out)

    # define database query for borehole report urls in jupiter db
    sql_pdf_out = f'''
        WITH tmp as ({shp_geom})
        SELECT DISTINCT    
            sd.url, b.boreholeno, bd.versionno,  
            concat(b.boreholeno, '_', bd.doctype, '_', bd.versionno) as bor_ver 
        FROM jupiter.storedoc sd
        LEFT JOIN jupiter.boredoc bd USING (fileid)
        LEFT JOIN jupiter.borehole b USING (boreholeid) 
        INNER JOIN tmp ON st_within(b.geom, tmp.geom)
        WHERE b.geom IS NOT NULL
            AND bd.doctype IN ('B', 'BG')
        ORDER BY b.boreholeno, bd.versionno
        ;
    '''
    print(sql_pdf_out)

    # define database query for borehole color in jupiter db
    borid_url = 'https://data.geus.dk/JupiterWWW/borerapport.jsp?borid'
    sql_jup_color_out = f'''
        WITH tmp as ({shp_geom})
        SELECT DISTINCT
            '<a href="' || '{borid_url}=' || b.boreholeid || '">' || replace(b.boreholeno, ' ', '') || '</a>' as url, 
            b.boreholeid, b.boreholeno,
            ls.top, ls.bottom, ls.rocksymbol, 
            cd.color_descr,	cd1.drilcol_descr,
            cd2.oldcolor_descr,	cd3.muncolor_descr
        FROM jupiter.lithsamp ls
        INNER JOIN jupiter.borehole b ON b.boreholeid = ls.boreholeid
        INNER JOIN tmp ON st_within(b.geom, tmp.geom)
        LEFT JOIN jupiter.boredoc bd ON bd.boreholeid = ls.boreholeid 
        LEFT JOIN 
            (
                SELECT c.code, c.longtext AS color_descr
                FROM jupiter.code c 
                WHERE c.codetype = 48
            ) AS cd ON cd.code = ls.color
        LEFT JOIN 
            (
                SELECT c.code, c.longtext AS drilcol_descr
                FROM jupiter.code c 
                WHERE c.codetype = 48
            ) AS cd1 ON cd1.code = ls.drillcolor
        LEFT JOIN 
            (
                SELECT c.code, c.longtext AS oldcolor_descr
                FROM jupiter.code c 
                WHERE c.codetype = 51
            ) AS cd2 ON cd2.code = ls.oldcolor
        LEFT JOIN 
            (
                SELECT c.code, c.longtext AS muncolor_descr
                FROM jupiter.code c 
                WHERE c.codetype = 79
            ) AS cd3 ON cd3.code = ls.munsellcolor
        WHERE b.geom IS NOT NULL
            AND COALESCE(ls.color, ls.munsellcolor, ls.oldcolor, ls.drillcolor) IS NOT NULL
        ORDER BY b.boreholeno, ls.top 
        ;
    '''
    print(sql_jup_color_out)

    return sql_borehole_out, sql_pdf_out, sql_jup_color_out


def merge_borehole_pdf(filename_input, urls_input, dgu_versions_input):
    skipped_dgus = []

    for i, (url, dgu_version) in enumerate(zip(urls_input, dgu_versions_input)):
        ite = i + 1

        print(
            f'\r{ite} report out of {len(urls_input)} ({round(ite / len(urls_input) * 100, 2)}%) url: {url}',
            end='',
            flush=True
        )

        pdf_merge_path = f"./{filename_input}_redox_pdf.pdf"
        pdf_merge_path_copy = f"./{filename_input}_redox_pdf_copy.pdf"

        try:
            merged_object = PdfFileMerger()
            if i > 0:
                merged_object.append(pdf_merge_path_copy)

            with requests.get(url, stream=True, allow_redirects=True) as r:
                if r.status_code == 200:
                    merged_object.append(PdfFileReader(ResponseStream(r.iter_content(64)), strict=False))
                    with open(pdf_merge_path, "wb") as pdf_merge:
                        merged_object.write(pdf_merge)
                else:
                    skipped_dgus.append(f'DGUnr:{dgu_version}, URL:{url}, ERROR:Could not connect to url')
        except Exception as e:
                skipped_dgus.append(f'DGUnr:{dgu_version}, URL:{url}, ERROR:{e}')
        else:
            shutil.copyfile(pdf_merge_path, pdf_merge_path_copy)

    print(f'\n \n The following borehole reports are skipped:')
    for skipped_dgu in skipped_dgus:
        print(f'\n{skipped_dgu}')


def create_pdf_jupiter_color(filename_input, df_jup_color_tmp_input):
    """
    descr:  group by dguno. and insert empty row and columnnames for each group
            source: https://stackoverflow.com/a/59623626/6129515
    :param filename_input:
    :param df_jup_color_tmp_input:
    :return:
    """
    df2 = pd.DataFrame([[''] * len(df_jup_color_tmp_input.columns), df_jup_color_tmp_input.columns],
                       columns=df_jup_color_tmp_input.columns)
    df_jup_color = (df_jup_color_tmp_input.groupby('boreholeno', group_keys=False, sort=False)
        .apply(lambda d: d.append(df2))
        .iloc[:-2]
        .reset_index(drop=True)
    )

    path_wkhtmltopdf = './requirements/wkhtmltopdf/bin/wkhtmltopdf.exe'
    pdf_config = pdf.configuration(wkhtmltopdf=path_wkhtmltopdf)

    pdf_options = {
        'page-size': 'Letter',
        'margin-top': '0.75in',
        'margin-right': '0.75in',
        'margin-bottom': '0.75in',
        'margin-left': '0.75in',
        'encoding': "UTF-8",
        'custom-header': [
            ('Accept-Encoding', 'gzip')
        ],
        'cookie': [
            ('cookie-name1', 'cookie-value1'),
            ('cookie-name2', 'cookie-value2'),
        ],
        'no-outline': None
    }

    html = df_jup_color.to_html(escape=False)  # escape=false keeps the html encoding of the urls
    filename_output = f'{filename_input}_jupiterredox'
    with open(f'./{filename_output}.html', "w", encoding="utf-8") as file:
        file.write(html)
    pdf.from_file(f'./{filename_output}.html', f'./{filename_output}.pdf',
                  configuration=pdf_config, options=pdf_options)

    if os.path.exists(f'./{filename_output}.html'):
        os.remove(f'./{filename_output}.html')


def color_columnnames(path_excel_input, path_excel):
    wb = openpyxl.load_workbook(filename=path_excel_input)
    ws = wb['Metadata_redoxpunkter']

    cell_color_dict = {
        'FF7DF26A': [
            'A1', 'B1', 'C1', 'D1', 'E1', 'F1'
        ],
        'FFFFE699': [
            'G1', 'H1', 'I1', 'J1', 'K1', 'L1', 'M1'
        ],
        'FFFCE4D6': [
            'S1', 'T1', 'U1', 'V1', 'W1',
            'AC1', 'AD1', 'AE1', 'AF1', 'AG1',
            'AM1', 'AN1', 'AO1', 'AP1', 'AQ1',
            'AW1', 'AX1', 'AY1', 'AZ1', 'BA1',
            'BG1', 'BH1', 'BI1', 'BJ1', 'BK1',
            'BQ1', 'BR1', 'BS1', 'BT1', 'BU1'
        ],
        'FFD0CECE': [
            'N1', 'O1', 'P1', 'Q1', 'R1',
            'X1', 'Y1', 'Z1', 'AA1', 'AB1',
            'AH1', 'AI1', 'AJ1', 'AK1', 'AL1',
            'AR1', 'AS1', 'AT1', 'AU1', 'AV1',
            'BB1', 'BC1', 'BD1', 'BE1', 'BF1',
            'BL1', 'BM1', 'BN1', 'BO1', 'BP1',
        ],
    }
    for color, cell_list in cell_color_dict.items():

        colorfill = openpyxl.styles.PatternFill(
            start_color=f'{color}',
            end_color=f'{color}',
            fill_type='solid'
        )

        for cell in cell_list:
            ws[f'{cell}'].fill = colorfill

    wb.save(path_excel)
    wb.close


def run_redox_borehole_color(path_shape):
    warnings.filterwarnings("ignore")

    filename = os.path.splitext(os.path.basename(path_shape))[0]

    # fetch database credentials
    ini_dir = r'F:/GKO/data/grukos/db_credentials/reader/reader.ini'
    ini_section = 'JUPITERREADER'
    pguser, pgpassword, pghost, pgport, pgdatabase = parse_db_credentials(ini_dir, ini_section)

    # fetch sql to:
    #   1) borehole information for excel spreadsheet,
    #   2) borehole reports for combined pdf and
    #   3) jupiter color descr for pdf.
    sql_borehole, sql_pdf, sql_jup_color = create_redox_sql(path_shape)

    # connect to the databases af extract dataframe from sql queries
    with connect_to_pg_db(pghost, pgport, pgdatabase, pguser, pgpassword) as con:
        df_borehole = pd.read_sql_query(sql_borehole, con=con)
        df_pdf_url = pd.read_sql_query(sql_pdf, con=con)
        df_jup_color = pd.read_sql_query(sql_jup_color, con=con)

    # # # CREATE CSV CONTAINING BOREHOLE INFORMATION # # #
    # df_borehole.to_csv(f'./{filename}_redox_boringer_GIS.csv', sep=',', index=False)
    # df_t = df_borehole.T
    # df_t.to_csv(f'./{filename}_redox_boringer.csv', sep=',', index=False, header=False)

    # # # CREATE MS EXCEL SPREADSHEET CONTAINING BOREHOLE INFORMATION # # #
    path_excel = f'./{filename}_redox_boringer.xlsx'
    df_borehole.to_excel(excel_writer=path_excel, sheet_name='Metadata_redoxpunkter', index=False)
    color_columnnames(path_excel, path_excel)  # COLOR COLUMNNAMES

    # # # CREATE PDF FROM COLOR DESCRIPTION IN JUPITER LITHSAMP # # #
    print('\nCompiling pdf from jupiter color descriptions.... \n')
    create_pdf_jupiter_color(filename, df_jup_color)

    # # # CREATE MERGED PDF FROM BOREHOLE DESCRIPTION REPORTS # # #
    print('\nCompiling borehole reports to a single pdf... \n')
    urls = df_pdf_url['url']
    dgu_versions = df_pdf_url['bor_ver']
    merge_borehole_pdf(filename, urls, dgu_versions)


# start timer
t = time.time()

path_shape = r'C:\Users\b028067\Downloads\test_redox_corrupt_pdf.shp'
run_redox_borehole_color(path_shape)

# end timer
elapsed = round(time.time() - t, 2)
print(f'\n{os.path.basename(__file__)} executed in {elapsed} s')
