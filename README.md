# Redox extract

## Description
This application is an interactive tool for extracting redox data 

### redox_borehole_color
borehole information, boreholereports and color asessments from the Jupiter database. 

### redox_clay_and_sand_thickness_from_terrain
To be written...

## Installation
None, the scripts must be run within python3. 

## Output
### redox_borehole_color
The script yields the following outputs where * is name of the input shapefile:
1) *_redox_pdf.pdf: All boreholereports merged, within the shp geometry.
2) *_jupiterredox.pdf: A table containing all color descriptions of boreholes, within the shp geometry. Boreholes that contains a boreholereport but not a color description are included.
3) *_redox_boringer.csv: Borehole information of the boreholes within the shp geometry. The borehole must have either a boreholereport or a colordescription to be included.

### redox_clay_and_sand_thickness_from_terrain
To be written...

## Dependencies
### redox_borehole_color
For the program to launch successfully,
it must be launched from the MST-GKO network with access to the Jupiter database. 
Furthermore .py requires wkhtmltopdf.exe, which is included in the script.

### redox_clay_and_sand_thickness_from_terrain
To be written...

## Help
Contact [Simon Makwarth](mailto:simak@mst.dk).

## Author and License
Redox extract is written by [Simon Makwarth](mailto:simak@mst.dk), and is 
licensed under the GNU Public License version 3 or later. See 
[LICENSE](LICENSE) for details.